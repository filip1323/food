package lukomski.filip.food;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.support.GenericConversionService;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;

import static org.junit.Assert.assertNotNull;

@Component
public class JsonUtils {

    @Autowired
    GenericConversionService conversionService;
    private HttpMessageConverter mappingJackson2HttpMessageConverter;
    public MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));
    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

        this.mappingJackson2HttpMessageConverter = Arrays.stream(converters)
                .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
                .findAny()
                .orElse(null);

        assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }


    public String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return prettyPrint(mockHttpOutputMessage.getBodyAsString());
//        mapper.readValue(input, Object.class);

    }

    private String prettyPrint(String json){
        try {
            ObjectMapper mapper = new ObjectMapper();
            Object jsonMapped = mapper.readValue(json, Object.class);
            String indented = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonMapped);
            return indented;
        } catch (IOException e) {
            e.printStackTrace();
            return json;
        }
    }
}
