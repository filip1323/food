package lukomski.filip.food;

import lukomski.filip.food.aspects.planner.Receipt;
import lukomski.filip.food.aspects.storage.ProductQuantity;
import lukomski.filip.food.aspects.supplies.Product;
import lukomski.filip.food.aspects.supplies.ProductFactory;
import lukomski.filip.food.aspects.supplies.QuantityStandard;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class DatabaseMocks {
    @Autowired
    ProductFactory productFactory;
    private Product ryzProduct;
    private Product kurczakProduct;
    private Product crunchyProduct;
    private Product mlekoProduct;

    public Collection<Product> products() {
        Collection<Product> products = new ArrayList<>();

        products.add(ryzProduct = productFactory.create("ryż", "", 7f, 80f, 0.6f));
        products.add(kurczakProduct = productFactory.create("kurczak", "", 1.3f, 0f, 21.5f));
        products.add(productFactory.create("pistacje", "", 45f, 27.5f, 20f));
        products.add(crunchyProduct = productFactory.create("crunchy", "", 13.8f, 65.8f, 5.2f));
        products.add(mlekoProduct = productFactory.create("mleko", "", 3.2f, 4.7f, 3.2f));

        return products;
    }

    public Product product() {
        return productFactory.create("ryż", "", 7f, 80f, 0.6f);
    }

    public List<Receipt> receipts() {
        List<Receipt> receipts = new ArrayList<>();
        receipts.add(new Receipt("Kurczak z ryżem")
                .add(ryzProduct, new ProductQuantity(QuantityStandard.WEIGHT, 0.1d))
                .add(kurczakProduct, new ProductQuantity(QuantityStandard.WEIGHT, 0.2d)));
        receipts.add(new Receipt("Płatki z mlekiem")
                .add(crunchyProduct, new ProductQuantity(QuantityStandard.WEIGHT, 0.09d))
                .add(mlekoProduct, new ProductQuantity(QuantityStandard.VOLUME, 0.2d)));
        return receipts;
    }

    public Receipt receipt(){
        return receipts().stream().findFirst().get();
    }
}
