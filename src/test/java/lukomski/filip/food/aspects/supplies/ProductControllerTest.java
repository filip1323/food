package lukomski.filip.food.aspects.supplies;

import lukomski.filip.food.DatabaseMocks;
import lukomski.filip.food.JsonUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.restdocs.payload.RequestFieldsSnippet;
import org.springframework.restdocs.payload.ResponseFieldsSnippet;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;
import java.util.Optional;

import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = "application-test.properties")
@AutoConfigureMockMvc
@AutoConfigureRestDocs(outputDir = "target/snippets", uriHost = "api", uriPort = 8080)
@ActiveProfiles("test")
public class ProductControllerTest {
  public static final String NO_DESCRIPTION = "";
  @Autowired
  private ProductController productController;
  @Autowired
  private ProductFacade productFacade;
  @Autowired
  private MockMvc mockMvc;
  @Autowired
  private JsonUtils jsonUtils;
  @Autowired
  private DatabaseMocks databaseMocks;


  @Test
  @DirtiesContext
  public void addProductSuccessfully() throws Exception {
    Product product = databaseMocks.product();
    this.mockMvc.perform(post("/product")
        .content(jsonUtils.json(product))
        .contentType(jsonUtils.contentType))
        .andDo(print()).andExpect(status().isCreated())
        .andExpect(content().string(containsString(product.getName())))
        .andDo(document("product@post",
            productRequestDescription(),
            productResponseDescription())
        );
  }

  @Test
  public void addProductUnsuccessfully() throws Exception {
    this.mockMvc.perform(post("/product"))
        .andDo(print()).andExpect(status().isBadRequest());
  }

  @Test
  @DirtiesContext
  public void getAllProducts() throws Exception {
    this.productFacade.getRepository().save(databaseMocks.products());
    this.mockMvc.perform(get("/product"))
        .andDo(print()).andExpect(status().isOk())
        .andExpect(jsonPath("$.length()", is(databaseMocks.products().size())))
        .andDo(document("product@get",
            responseFields(fieldWithPath("[]").description("Array of products"))));
  }

  @Test
  @DirtiesContext
  public void getProductByIdSuccessfully() throws Exception {
    final Integer PRODUCT_ID = 3;
    this.productFacade.getRepository().save(databaseMocks.products());
    this.mockMvc.perform(get("/product/" + PRODUCT_ID))
        .andDo(print()).andExpect(status().isOk())
        .andExpect(jsonPath("$.id", is(PRODUCT_ID)))
        .andDo(document("product^id@get",
            productResponseDescription()));
  }

  @Test
  public void getProductByIdUnsuccessfully() throws Exception {
    final Integer PRODUCT_ID = 3;
    this.mockMvc.perform(get("/product/" + PRODUCT_ID))
        .andDo(print()).andExpect(status().isNotFound());
  }


  @Test
  @DirtiesContext
  public void updateProductByIdSuccessfully() throws Exception {
    this.productFacade.getRepository().save(databaseMocks.products());
    Product update = databaseMocks.product();
    update.setName("updated");

    final Integer PRODUCT_ID = 3;
    this.mockMvc.perform(put("/product/" + PRODUCT_ID)
        .content(jsonUtils.json(update))
        .contentType(jsonUtils.contentType))
        .andDo(print()).andExpect(status().isOk())
        .andExpect(jsonPath("$.name", is(update.getName())))
        .andDo(document("product^id@put",
            productRequestDescription(),
            productResponseDescription()));
  }

  @Test
  public void updateProductByIdUnsuccessfully() throws Exception {
    final Integer PRODUCT_ID = 3;
    this.mockMvc.perform(put("/product/" + PRODUCT_ID)
        .content(jsonUtils.json(databaseMocks.product()))
        .contentType(jsonUtils.contentType))
        .andDo(print()).andExpect(status().isNotFound());
  }


  @Test
  @DirtiesContext
  public void deleteProductByIdSuccessfully() throws Exception {
    final Long PRODUCT_ID = 3L;

    this.productFacade.getRepository().save(databaseMocks.products());

    Optional<Product> insertedProduct = this.productFacade.getRepository().findById(PRODUCT_ID);
    assertTrue(insertedProduct.isPresent());

    this.mockMvc.perform(delete("/product/" + PRODUCT_ID))
        .andDo(print()).andExpect(status().isOk())
        .andDo(document("product^id@delete"));

    Optional<Product> deletedProduct = this.productFacade.getRepository().findById(PRODUCT_ID);
    assertTrue(!deletedProduct.isPresent());
  }

  @Test
  public void deleteProductByIdUnsuccessfully() throws Exception {
    final Integer PRODUCT_ID = 3;
    this.mockMvc.perform(delete("/product/" + PRODUCT_ID))
        .andDo(print()).andExpect(status().isNotFound());
  }

  private RequestFieldsSnippet productRequestDescription() {
    return requestFields(
        fieldWithPath("id").ignored(),
        fieldWithPath("name").description(NO_DESCRIPTION),
        fieldWithPath("description").description(NO_DESCRIPTION),
        fieldWithPath("macronutrients.fatPerUnit").description("0 <= [value] <= 100"),
        fieldWithPath("macronutrients.carboPerUnit").description("0 <= [value] <= 100"),
        fieldWithPath("macronutrients.proteinPerUnit").description("0 <= [value] <= 100"));
  }

  private ResponseFieldsSnippet productResponseDescription() {
    return responseFields(
        fieldWithPath("id").description("id of newly saved resource"),
        fieldWithPath("name").description(NO_DESCRIPTION),
        fieldWithPath("description").description(NO_DESCRIPTION),
        fieldWithPath("macronutrients.fatPerUnit").description("fat content in 100g"),
        fieldWithPath("macronutrients.carboPerUnit").description("carbo content in 100g"),
        fieldWithPath("macronutrients.proteinPerUnit").description("protein content in 100g"));
  }

}