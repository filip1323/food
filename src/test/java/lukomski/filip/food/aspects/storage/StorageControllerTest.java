package lukomski.filip.food.aspects.storage;

import lukomski.filip.food.DatabaseMocks;
import lukomski.filip.food.JsonUtils;
import lukomski.filip.food.aspects.planner.Receipt;
import lukomski.filip.food.aspects.planner.ReceiptController;
import lukomski.filip.food.aspects.planner.ReceiptFacade;
import lukomski.filip.food.aspects.supplies.ProductFacade;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.restdocs.payload.RequestFieldsSnippet;
import org.springframework.restdocs.payload.ResponseFieldsSnippet;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = "application-test.properties")
@AutoConfigureMockMvc
@AutoConfigureRestDocs(outputDir = "target/snippets", uriHost = "api")
@ActiveProfiles("test")
public class StorageControllerTest {

    private static final String NO_DESCRIPTION = "";
    @Autowired
    private ReceiptController receiptController;
    @Autowired
    private ReceiptFacade receiptFacade;
    @Autowired
    private ProductFacade productFacade;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private JsonUtils jsonUtils;
    @Autowired
    private DatabaseMocks databaseMocks;
    private final Long RECEIPT_ID = 1L;


    @Before
    public void beforeClass() {
        productFacade.getRepository().save(databaseMocks.products());
    }

    @Test
    @DirtiesContext
    public void deleteReceiptByIdSuccessfully() throws Exception {
        this.receiptFacade.getRepository().save(databaseMocks.receipts());

        Optional<Receipt> insertedReceipt = this.receiptFacade.getRepository().findById(RECEIPT_ID);
        assertTrue(insertedReceipt.isPresent());

        this.mockMvc.perform(delete("/receipt/" + RECEIPT_ID))
                .andDo(print()).andExpect(status().isOk())
                .andDo(document("receipt^id@delete"));

        Optional<Receipt> deletedReceipt = this.receiptFacade.getRepository().findById(RECEIPT_ID);
        assertTrue(!deletedReceipt.isPresent());
    }

    @Test
    public void deleteReceiptByIdUnsuccessfully() throws Exception {
        final Integer RECEIPT_ID = 3;
        this.mockMvc.perform(delete("/receipt/" + RECEIPT_ID))
                .andDo(print()).andExpect(status().isNotFound());
    }

    @Test
    @DirtiesContext
    public void getAllReceipts() throws Exception {
        this.receiptFacade.getRepository().save(databaseMocks.receipts());
        this.mockMvc.perform(get("/receipt"))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$.length()", is(databaseMocks.receipts().size())))
                .andDo(document("receipt@get",
                        responseFields(fieldWithPath("[]").description("Array of receipts"))));
    }

    @Test
    @DirtiesContext
    public void getReceiptByIdSuccessfully() throws Exception {
        final Integer RECEIPT_ID = 1;
        this.receiptFacade.getRepository().save(databaseMocks.receipts());
        this.mockMvc.perform(get("/receipt/" + RECEIPT_ID))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(RECEIPT_ID)))
                .andDo(document("receipt^id@get",
                        responseFields(
                                fieldWithPath("id").description("id of newly saved resource"),
                                fieldWithPath("name").description(NO_DESCRIPTION),
                                fieldWithPath("shoppingList[].product").description("product as in /product service"),
                                fieldWithPath("shoppingList[].productQuantity.value").description("module of quantity"),
                                fieldWithPath("shoppingList[].productQuantity.standard").description("quantity standard with all metadata, as in /config/quantityStandards"))));
    }

    @Test
    public void getReceiptByIdUnsuccessfully() throws Exception {
        final Integer RECEIPT_ID = 3;
        this.mockMvc.perform(get("/receipt/" + RECEIPT_ID))
                .andDo(print()).andExpect(status().isNotFound());
    }


    @Test
    @DirtiesContext
    public void addReceiptSuccessfully() throws Exception {
        Receipt receipt = databaseMocks.receipt();
        this.mockMvc.perform(post("/receipt")
                .content(jsonUtils.json(receipt))
                .contentType(jsonUtils.contentType))
                .andDo(print()).andExpect(status().isCreated())
                .andExpect(content().string(containsString(receipt.getName())))
                .andDo(document("receipt@post",
                        receiptRequestDescription(),
                        receiptResponseDescription())
                );
    }

    @Test
    public void addReceiptUnsuccessfully() throws Exception {
        this.mockMvc.perform(post("/receipt"))
                .andDo(print()).andExpect(status().isBadRequest());
    }


    private RequestFieldsSnippet receiptRequestDescription() {
        return requestFields(
                fieldWithPath("id").ignored(),
                fieldWithPath("name").description(NO_DESCRIPTION),
                fieldWithPath("shoppingList").description("array of product servings"));
    }

    private ResponseFieldsSnippet receiptResponseDescription() {
        return responseFields(
                fieldWithPath("id").description("id of newly saved resource"),
                fieldWithPath("name").description(NO_DESCRIPTION),
                fieldWithPath("shoppingList").description("array of product servings"));
    }

}