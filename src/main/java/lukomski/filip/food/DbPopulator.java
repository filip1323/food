package lukomski.filip.food;

import lukomski.filip.food.aspects.planner.Receipt;
import lukomski.filip.food.aspects.planner.ReceiptFacade;
import lukomski.filip.food.aspects.storage.ProductQuantity;
import lukomski.filip.food.aspects.supplies.Product;
import lukomski.filip.food.aspects.supplies.ProductFacade;
import lukomski.filip.food.aspects.supplies.ProductFactory;
import lukomski.filip.food.aspects.supplies.QuantityStandard;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;

@Component
public class DbPopulator {
    @Autowired
    private ProductFactory productFactory;

    @Autowired
    private ProductFacade productFacade;

    @Autowired
    private ReceiptFacade receiptFacade;

    private Product ryzProduct;
    private Product kurczakProduct;
    private Product pistacjeProduct;
    private Product crunchyProduct;
    private Product mlekoProduct;

    public void clearDb() {
        Iterable<Receipt> receipts = receiptFacade.getRepository().findAll();
        receiptFacade.getRepository().delete(receipts);

        Iterable<Product> products = productFacade.getRepository().findAll();
        productFacade.getRepository().delete(products);
    }

    public void populateDb() {
        clearDb();
        addProducts();
        addReceipts();
    }

    public void addReceipts() {
        receiptFacade.add(new Receipt("Kurczak z ryżem")
                .add(ryzProduct, new ProductQuantity(QuantityStandard.WEIGHT, 0.1d))
                .add(kurczakProduct, new ProductQuantity(QuantityStandard.WEIGHT, 0.2d)));
        receiptFacade.add(new Receipt("Płatki z mlekiem")
                .add(crunchyProduct, new ProductQuantity(QuantityStandard.WEIGHT, 0.09d))
                .add(mlekoProduct, new ProductQuantity(QuantityStandard.VOLUME, 0.2d)));
    }


    public void addProducts() {
        productFacade.getRepository().save(products());
    }


    public Collection<Product> products() {
        Collection<Product> products = new ArrayList<>();

        products.add(ryzProduct = productFactory.create("ryż", "", 7f, 80f, 0.6f));
        products.add(kurczakProduct = productFactory.create("kurczak", "", 1.3f, 0f, 21.5f));
        products.add(pistacjeProduct = productFactory.create("pistacje", "", 45f, 27.5f, 20f));
        products.add(crunchyProduct = productFactory.create("crunchy", "", 13.8f, 65.8f, 5.2f));
        products.add(mlekoProduct = productFactory.create("mleko", "", 3.2f, 4.7f, 3.2f));

        return products;
    }
}
