package lukomski.filip.food;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

@CrossOrigin
@Controller
public class AppController {

    private DbPopulator dbPopulator;

    @Autowired
    public AppController(DbPopulator dbPopulator) {
        this.dbPopulator = dbPopulator;
    }

    @GetMapping("ping")
    public ResponseEntity<Boolean> ping(){
        return new ResponseEntity<>(Boolean.TRUE, HttpStatus.OK);
    }

    @GetMapping("db/clear")
    public ResponseEntity<Void> clearDb(){
        dbPopulator.clearDb();
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("db/populate")
    public ResponseEntity<Void> populateDb() {
        dbPopulator.populateDb();
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
