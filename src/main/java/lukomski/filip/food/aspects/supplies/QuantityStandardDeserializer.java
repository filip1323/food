package lukomski.filip.food.aspects.supplies;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import org.apache.log4j.Logger;

import java.io.IOException;

class QuantityStandardDeserializer extends StdDeserializer<QuantityStandard> {
    private static final Logger LOG = Logger.getLogger(QuantityStandardDeserializer.class);

    QuantityStandardDeserializer() {
        super(QuantityStandard.class);
    }

    @Override
    public QuantityStandard deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        String id = node.get("id").textValue();

        for (QuantityStandard quantityStandard : QuantityStandard.values()) {
            if (quantityStandard.name().equals(id)) {
                return quantityStandard;
            }
        }
        LOG.warn("Could't deserialize QuantityStandard");
        return QuantityStandard.NUMBER;
    }

}
