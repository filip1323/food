package lukomski.filip.food.aspects.supplies;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
public class ProductFactory {
    private MacronutrientsFactory macronutrientsFactory;

    @Autowired
    public ProductFactory(MacronutrientsFactory macronutrientsFactory) {
        this.macronutrientsFactory = macronutrientsFactory;
    }


    public Product create(String name,
                          String description,
                          Float proteinPerUnit,
                          Float carboPerUnit,
                          Float fatPerUnit){

        Product product = new Product();
        product.setName(name);
        product.setDescription(description);

        Macronutrients nutrients = macronutrientsFactory.create(proteinPerUnit, carboPerUnit, fatPerUnit);
        product.setMacronutrients(nutrients);

        return product;
    }


    public Product update(Product updated, Product incoming) {
        updated.setName(incoming.getName());
        updated.setDescription(incoming.getDescription());
//        updated.setFatPerUnit(incoming.getFatPerUnit());
//        updated.setCarboPerUnit(incoming.getCarboPerUnit());
//        updated.setProteinPerUnit(incoming.getProteinPerUnit());
        return updated;
    }
}
