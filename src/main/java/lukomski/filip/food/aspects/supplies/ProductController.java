package lukomski.filip.food.aspects.supplies;

import lukomski.filip.food.aspects.form.Form;
import lukomski.filip.food.aspects.form.ValidationResponse;
import lukomski.filip.food.aspects.form.validators.LengthValidator;
import lukomski.filip.food.aspects.form.validators.NumberValueValidator;
import lukomski.filip.food.aspects.form.validators.OnlyLettersAndPunctValidator;
import lukomski.filip.food.config.auth.Role;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionException;
import org.springframework.core.convert.support.GenericConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
//import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("product")
public class ProductController {
    private static final Logger LOG = Logger.getLogger(ProductController.class);
    private ProductFacade facade;
    private Form productForm;


    @Autowired
    public ProductController(ProductFacade facade) {
        this.facade = facade;
    }

    @PostConstruct
    public void onInitialization() {
        productForm = new Form();
        productForm.add("name", String.class, false, new LengthValidator(0, 160), new OnlyLettersAndPunctValidator())
                .add("fatPerUnit", Double.class, false,new NumberValueValidator(0d, 100d))
                .add("carboPerUnit", Double.class, false,new NumberValueValidator(0d, 100d))
                .add("proteinPerUnit", Double.class, false,new NumberValueValidator(0d, 100d))
                .add("defaultStandard", String.class, false)
                .add("description", String.class, false, new OnlyLettersAndPunctValidator());
    }

//    @Secured(Role.CREATE)
    @PostMapping
    public ResponseEntity<Product> add(@RequestBody Product newProduct) {
        return new ResponseEntity<>(facade.add(newProduct), HttpStatus.CREATED);
    }

//    @Secured(Role.READ)
    @GetMapping
    public ResponseEntity<Iterable<Product>> get() {
        Iterable<Product> products = facade.getAll();
        HttpStatus status = products.iterator().hasNext() ?
                HttpStatus.OK : HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(products, status);
    }

//    @Secured(Role.READ)
    @GetMapping(value = "{id}")
    public ResponseEntity<Product> get(@PathVariable("id") Long id) {
        Optional<Product> currentProduct = facade.getRepository().findById(id);

        //noinspection OptionalIsPresent
        if (currentProduct.isPresent()) {
            return new ResponseEntity<>(currentProduct.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

//    @Secured(Role.UPDATE)
    @PutMapping(value = "{id}")
    public ResponseEntity<Product> put(@PathVariable("id") Long id, @RequestBody Product product) {
        Optional<Product> currentProduct = facade.getRepository().findById(id);

        if (currentProduct.isPresent()) {
            facade.update(currentProduct.get(), product);
            return new ResponseEntity<>(currentProduct.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

//    @Secured(Role.DELETE)
    @DeleteMapping(value = "{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
        Optional<Product> currentProduct = facade.getRepository().findById(id);

        if (currentProduct.isPresent()) {
            facade.getRepository().delete(currentProduct.get());
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    @Autowired
    GenericConversionService conversionService;

//    @Secured(Role.READ)
    @PostMapping(value = "validate/{fieldName}")
    public ResponseEntity<ValidationResponse> validate(@PathVariable("fieldName") String fieldName, @RequestBody(required = false) String value) {
        LOG.info("Validating " + fieldName + " with value " + value);
        Class<?> clazz = productForm.getFieldClass(fieldName);
        Optional<?> valueWrapper = Optional.ofNullable(value);
        try {
            if (valueWrapper.isPresent()) {
                valueWrapper = Optional.of(conversionService.convert(value, clazz));
            }
            return new ResponseEntity<>(productForm.validate(fieldName, valueWrapper), HttpStatus.OK);
        } catch (ConversionException ex) {
            return new ResponseEntity<>(ValidationResponse.invalidResponse("Błedny format danych"), HttpStatus.OK);
        }
    }


}
