package lukomski.filip.food.aspects.supplies;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private String description;

    @Embedded
    private Macronutrients macronutrients;
}
