package lukomski.filip.food.aspects.supplies;

import org.springframework.stereotype.Component;

@Component
public class MacronutrientsFactory {

    public Macronutrients create(Float proteinPerUnit,
                                 Float carboPerUnit,
                                 Float fatPerUnit) {
        Macronutrients nutrients = new Macronutrients();
        nutrients.setProteinPerUnit(proteinPerUnit);
        nutrients.setCarboPerUnit(carboPerUnit);
        nutrients.setFatPerUnit(fatPerUnit);
        return nutrients;
    }
}
