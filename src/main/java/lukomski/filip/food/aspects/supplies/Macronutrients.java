package lukomski.filip.food.aspects.supplies;

import lombok.Data;

@Data
public class Macronutrients {

    private Float fatPerUnit;
    private Float carboPerUnit;
    private Float proteinPerUnit;
}
