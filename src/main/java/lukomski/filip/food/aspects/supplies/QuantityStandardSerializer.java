package lukomski.filip.food.aspects.supplies;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

class QuantityStandardSerializer extends StdSerializer<QuantityStandard> {
    QuantityStandardSerializer() {
        super(QuantityStandard.class);
    }

    @Override
    public void serialize(QuantityStandard quantityStandard, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField("id", quantityStandard.getId());
        jsonGenerator.writeStringField("name", quantityStandard.getName());
        jsonGenerator.writeStringField("unit", quantityStandard.getUnit());
        jsonGenerator.writeEndObject();
    }

}
