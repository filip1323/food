package lukomski.filip.food.aspects.supplies;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductFacade {

    @Getter
    private ProductRepository repository;

    private ProductFactory factory;

    @Autowired
    public ProductFacade(ProductRepository repository, ProductFactory factory) {
        this.repository = repository;
        this.factory = factory;
    }

    public Product add(Product product) {
        return repository.save(product);
    }

    Iterable<Product> getAll() {
        return repository.findAll();
    }

    Product update(Product existing, Product incoming){
        factory.update(existing, incoming);
        repository.save(existing);
        return existing;
    }
}
