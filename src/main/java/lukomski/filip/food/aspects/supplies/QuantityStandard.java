package lukomski.filip.food.aspects.supplies;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

//@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@JsonDeserialize(using = QuantityStandardDeserializer.class)
@JsonSerialize(using = QuantityStandardSerializer.class)
public enum QuantityStandard {
    NUMBER("Ilość", ""),
    WEIGHT("Masa", "kg"),
    VOLUME("Objętość", "l");

    private final String id;
    private final String name;
    private final String unit;

    QuantityStandard(String name, String unit) {
        this.id = name();
        this.name = name;
        this.unit = unit;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUnit() {
        return unit;
    }

    @Override
    public String toString() {
        return name();
    }
}
