package lukomski.filip.food.aspects.form;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ValidationResponse {
    private Boolean valid;
    private List<String> responseMessages;
    private Boolean allowEmptyValue;
    private Boolean isEmpty;

    private ValidationResponse() {
        this.valid = true;
        this.responseMessages = new ArrayList<>();
    }

    private ValidationResponse(String responseMessage) {
        this.valid = false;
        this.responseMessages = new ArrayList<>();
        this.responseMessages.add(responseMessage);
    }

    public static ValidationResponse merge(List<ValidationResponse> responses, Boolean allowEmptyValue, Boolean isEmpty) {
        ValidationResponse mergedResponse = ValidationResponse.validResponse();
        ArrayList<String> mergedMessages = new ArrayList<String>();
        for (ValidationResponse response : responses) {
            mergedResponse.valid = mergedResponse.valid && response.valid;
            if (!response.valid && !response.responseMessages.isEmpty()) {
                mergedMessages.add(response.responseMessages.iterator().next());
            }
        }
        mergedResponse.responseMessages.addAll(mergedMessages);
        mergedResponse.allowEmptyValue = allowEmptyValue;
        mergedResponse.isEmpty = isEmpty;
        return mergedResponse;
    }

    public static ValidationResponse validResponse() {
        return new ValidationResponse();
    }

    public static ValidationResponse invalidResponse(String responseMessage) {
        return new ValidationResponse(responseMessage);
    }
}
