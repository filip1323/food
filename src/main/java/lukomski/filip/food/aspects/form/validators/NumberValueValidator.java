package lukomski.filip.food.aspects.form.validators;

import lukomski.filip.food.aspects.form.ValidationResponse;
import lukomski.filip.food.aspects.form.Validator;

public class NumberValueValidator implements Validator<Double> {
    private final Double min;
    private final Double max;

    public NumberValueValidator(Double min, Double max) {
        this.min = min;
        this.max = max;
    }

    @Override
    public ValidationResponse validate(String fieldName, Double value) {
        if (value < min) {
            return invalid("Minimalna wartość to " + min);
        } else if (value > max) {
            return invalid("Maksymalna wartość to " + max);
        } else {
            return valid();
        }
    }
}
