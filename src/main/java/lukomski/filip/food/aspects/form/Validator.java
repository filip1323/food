package lukomski.filip.food.aspects.form;

@FunctionalInterface
public interface Validator<T> {

    ValidationResponse validate(String fieldName, T value);

    default ValidationResponse valid() {
        return ValidationResponse.validResponse();
    }

    default ValidationResponse invalid(String message) {
        return ValidationResponse.invalidResponse(message);
    }
}
