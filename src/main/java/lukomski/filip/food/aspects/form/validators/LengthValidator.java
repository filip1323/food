package lukomski.filip.food.aspects.form.validators;

import lukomski.filip.food.aspects.form.ValidationResponse;
import lukomski.filip.food.aspects.form.Validator;

public class LengthValidator implements Validator<String> {
    private final Integer minLength;
    private final Integer maxLength;

    public LengthValidator(Integer minLength, Integer maxLength) {
        this.minLength = minLength;
        this.maxLength = maxLength;
    }

    @Override
    public ValidationResponse validate(String fieldName, String value) {
        Integer length = value.length();
        if (length < minLength) {
            return invalid("Minimalna liczba znaków  to " + minLength);
        } else if (length > maxLength) {
            return invalid("Maksymalna liczba znaków to " + maxLength);
        } else {
            return valid();
        }
    }
}
