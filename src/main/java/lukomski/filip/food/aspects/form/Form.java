package lukomski.filip.food.aspects.form;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class Form {

    private Map<String, FormControl<?>> formControls = new HashMap<>();

    public <T> Form add(String name, Class<T> clazz) {
        return this.add(name, clazz, true);
    }

    public <T> Form add(String name, Class<T> clazz, Boolean allowEmptyValue) {
        @SuppressWarnings("unchecked")
        Validator<T>[] validators = new Validator[]{};
        return this.add(name, clazz, allowEmptyValue, validators);
    }

    public <T> Form add(String name, Class<T> clazz, Boolean allowEmptyValue, Validator<T>... validators) {
        FormControl<T> control = new FormControl<>(name, clazz, validators).setAllowEmptyValue(allowEmptyValue);
        formControls.put(name, control);
        return this;
    }

    public Class<?> getFieldClass(String fieldName) {
        return formControls.get(fieldName).getClazz();
    }

    public ValidationResponse validate(String fieldName, Optional<?> value) {
        return formControls.get(fieldName).validate(fieldName, value);
    }

}
