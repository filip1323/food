package lukomski.filip.food.aspects.form.validators;

import lukomski.filip.food.aspects.form.ValidationResponse;
import lukomski.filip.food.aspects.form.Validator;

public class OnlyLettersAndPunctValidator implements Validator<String> {

    @Override
    public ValidationResponse validate(String fieldName, String value) {
        if (!value.matches("^[\\p{L} \\.,]+$")) {
            return invalid("Dozwolone są tylko małe i wielkie litery");
        } else {
            return valid();
        }
    }
}
