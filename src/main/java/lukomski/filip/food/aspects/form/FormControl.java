package lukomski.filip.food.aspects.form;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class FormControl<T> {
    private final String name;
    private Class<T> clazz;
    private final Validator<T>[] validators;
    private Boolean allowEmptyValue = true;

    public FormControl(String name, Class<T> clazz, Validator<T>[] validators) {
        this.name = name;
        this.clazz = clazz;
        this.validators = validators;
    }

    public ValidationResponse validate(String fieldName, Optional<?> value) {
        List<ValidationResponse> responses = new ArrayList<>();
        if (value.isPresent()) {
            for (Validator<T> validator : validators) {
                T castedValue = clazz.cast(value.get());
                responses.add(validator.validate(fieldName, castedValue));
            }
        }
        return ValidationResponse.merge(responses, allowEmptyValue, !value.isPresent());
    }

    public Class<T> getClazz() {
        return clazz;
    }

    public FormControl<T> setAllowEmptyValue(Boolean allow) {
        this.allowEmptyValue = allow;
        return this;
    }
}
