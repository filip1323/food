package lukomski.filip.food.aspects;

import com.fasterxml.jackson.databind.JsonNode;
import lukomski.filip.food.aspects.supplies.QuantityStandard;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("config")
public class Config {

    @GetMapping("quantityStandards")
    public ResponseEntity<QuantityStandard[]> getQuantityStandard(){
        return new ResponseEntity<>(QuantityStandard.values(), HttpStatus.OK);
    }
}
