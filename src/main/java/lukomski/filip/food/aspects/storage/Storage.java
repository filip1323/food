package lukomski.filip.food.aspects.storage;

import lombok.Getter;
import lombok.Setter;
import lukomski.filip.food.aspects.supplies.Product;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Storage {

    private static final int REVERSE = -1;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Getter
    @Setter
    private String name;
    @Getter
    private ArrayList<StorageableUnit> storageables = new ArrayList<>();

//    Collection<StorageableUnit> getMatching(Product product){
//        return getSorted(storageables.stream().filter(unit -> unit.getProduct().equals(product)).collect(Collectors.toList()));
//    }
//
//    Collection<StorageableUnit> getMatching(List<Product> products){
//        List<StorageableUnit> storageables = new ArrayList<>();
//        products.forEach(product -> storageables.addAll(this.getMatching(product)));
//        return getSorted(storageables);
//    }

    Storage put(StorageableUnit unit){
        storageables.add(unit);
        return this;
    }

    Storage put(Collection<StorageableUnit> units){
        storageables.addAll(units);
        return this;
    }

//    //TODO needs edge case test
//    Collection<StorageableUnit> getExpiring(Date expirationDate){
//        return getSorted(storageables.stream().filter(unit -> unit.getExpirationDate().before(expirationDate)).collect(Collectors.toList()));
//    }
//
//    //TODO needs order case test
//    private Collection<StorageableUnit> getSorted(Collection<StorageableUnit> units){
//        return units.stream().sorted((o1, o2) -> REVERSE * o1.getComparatorDate().compareTo(o2.getComparatorDate())).collect(Collectors.toList());
//    }

}
