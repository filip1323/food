package lukomski.filip.food.aspects.storage;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lukomski.filip.food.aspects.supplies.QuantityStandard;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
public class ProductQuantity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    private Long id;
    private QuantityStandard standard;
    private Double value;

    protected ProductQuantity() {
    }

    public ProductQuantity(QuantityStandard standard, Double value) {
        this.standard = standard;
        this.value = value;
    }
}
