package lukomski.filip.food.aspects.storage;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("storage")
public class StorageContoller {

    private StorageFacade facade;

    @PostMapping
    public ResponseEntity<Storage> add(String name){

        if(facade.getRepository().findByName(name).isPresent()){
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }else {
            Storage newStorage = new Storage();
            newStorage.setName(name);
            return new ResponseEntity<>(facade.add(newStorage), HttpStatus.CREATED);
        }
    }

    @GetMapping(value = "{id}")
    public ResponseEntity<Storage> get(@PathVariable("id") Long id){
        Optional<Storage> currentStorage = facade.getRepository().findById(id);

        if(currentStorage.isPresent()){
            return new ResponseEntity<>(currentStorage.get(), HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(value = "{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Long id){
        Optional<Storage> currentStorage = facade.getRepository().findById(id);

        if(currentStorage.isPresent()){
            facade.getRepository().delete(currentStorage.get());
            return new ResponseEntity<>(HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
