package lukomski.filip.food.aspects.storage;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface StorageRepository extends CrudRepository<Storage, Long> {
    Optional<Storage> findById(Long id);

    Optional<Storage> findByName(String name);
}
