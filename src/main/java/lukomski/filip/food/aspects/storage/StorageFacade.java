package lukomski.filip.food.aspects.storage;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StorageFacade {
    @Getter
    private StorageRepository repository;

    @Autowired
    public StorageFacade(StorageRepository repository) {
        this.repository = repository;
    }

    Storage add(Storage storage){
        return repository.save(storage);
    }
}
