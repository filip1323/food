package lukomski.filip.food.aspects.storage;

import org.springframework.data.repository.CrudRepository;

public interface StorageableUnitRepository extends CrudRepository<StorageableUnit, Long> {
}
