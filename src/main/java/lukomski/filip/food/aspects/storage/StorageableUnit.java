package lukomski.filip.food.aspects.storage;

import lombok.Data;
import lukomski.filip.food.aspects.supplies.Product;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
public class StorageableUnit {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne(cascade = CascadeType.ALL)
    private Product product;
    @OneToOne(cascade = CascadeType.ALL)
    private ProductQuantity quantity;
    private Date arrivingDate;
    private Date openingTime;

//    Date getExpirationDate(){
//        return Date.from(
//                getOpeningTime()
//                .toInstant()
//                .plus(product.getAverageTimeOfOutdate()));
//    }
//
//    Date getOpeningTime(){
//        if(product.getOpenSinceArrival()){
//            return arrivingDate;
//        }else{
//            return openingTime;
//        }
//    }

//    Date getComparatorDate(){
//        if(getOpeningTime() != null){
//            return getExpirationDate();
//        }else{
//            return arrivingDate;
//        }
//    }
}
