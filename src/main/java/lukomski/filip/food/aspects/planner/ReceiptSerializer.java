package lukomski.filip.food.aspects.planner;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import lukomski.filip.food.aspects.storage.ProductQuantity;
import lukomski.filip.food.aspects.supplies.Product;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class ReceiptSerializer extends StdSerializer<Receipt> {

    public ReceiptSerializer() {
        super(Receipt.class);
    }

    @Override
    public void serialize(Receipt receipt, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();

        if (receipt.getId() != null) {
            jsonGenerator.writeNumberField("id", receipt.getId());
        } else {
            jsonGenerator.writeNullField("id");
        }

        jsonGenerator.writeStringField("name", receipt.getName());

        ArrayNode shoppingList = serializeShoppingList(receipt);
        jsonGenerator.writeObjectField("shoppingList", shoppingList);

        jsonGenerator.writeEndObject();
    }

    private ArrayNode serializeShoppingList(Receipt receipt) {
        Hibernate.initialize(receipt.getReceiptItems());
        List<ReceiptItem> receiptItems = receipt.getReceiptItems().entrySet().stream().map(this::transform).collect(Collectors.toList());
        ObjectMapper mapper = new ObjectMapper();
        ArrayNode array = mapper.createArrayNode();
        for (ReceiptItem receiptItem : receiptItems) {
            array.add(mapper.valueToTree(receiptItem));
        }
        return array;
    }


    private ReceiptItem transform(Map.Entry<Product, ProductQuantity> entry) {
        return new ReceiptItem(entry.getKey(), entry.getValue());
    }
}
