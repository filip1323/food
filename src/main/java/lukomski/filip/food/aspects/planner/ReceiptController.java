package lukomski.filip.food.aspects.planner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("receipt")
public class ReceiptController {

    private final ReceiptFacade facade;

    @Autowired
    public ReceiptController(ReceiptFacade facade) {
        this.facade = facade;
    }

    @PostMapping
    public ResponseEntity<Receipt> add(@RequestBody Receipt newReceipt) {
        return new ResponseEntity<>(facade.add(newReceipt), HttpStatus.CREATED);
    }


    @PutMapping(value = "{id}")
    public ResponseEntity<Receipt> put(@PathVariable("id") Long id, @RequestBody Receipt receipt) {
        Optional<Receipt> currentReceipt = facade.getRepository().findById(id);

        if (currentReceipt.isPresent()) {
            facade.update(currentReceipt.get(), receipt);
            return new ResponseEntity<>(currentReceipt.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    @GetMapping
    public ResponseEntity<Iterable<Receipt>> get() {
        Iterable<Receipt> receipts = facade.getAll();
        HttpStatus status = receipts.iterator().hasNext() ?
                HttpStatus.OK : HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(receipts, status);
    }

    @GetMapping(value = "{id}")
    public ResponseEntity<Receipt> get(@PathVariable("id") Long id) {
        Optional<Receipt> currentReceipt = facade.getRepository().findById(id);

        //noinspection OptionalIsPresent
        if (currentReceipt.isPresent()) {
            return new ResponseEntity<>(currentReceipt.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(value = "{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
        Optional<Receipt> currentReceipt = facade.getRepository().findById(id);

        if (currentReceipt.isPresent()) {
            facade.getRepository().delete(currentReceipt.get());
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
