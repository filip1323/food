package lukomski.filip.food.aspects.planner;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import lukomski.filip.food.aspects.storage.ProductQuantity;
import lukomski.filip.food.aspects.supplies.Product;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

@Entity
@Data
@JsonDeserialize(using = ReceiptDeserializer.class)
@JsonSerialize(using = ReceiptSerializer.class)
public class Receipt {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JsonIgnore
    private Map<Product, ProductQuantity> receiptItems = new HashMap<>();

    protected Receipt() {
    }

    public Receipt(String name) {
        this.name = name;
    }

    public Receipt add(Product product, ProductQuantity expectedQuantity) {
        //TODO merge with existing pair, right now it will replace existing
        receiptItems.put(product, expectedQuantity);
        return this;
    }

    public Receipt remove(Product product) {
        receiptItems.remove(product);
        return this;
    }
}
