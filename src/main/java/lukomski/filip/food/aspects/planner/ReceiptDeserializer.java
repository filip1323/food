package lukomski.filip.food.aspects.planner;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import lukomski.filip.food.aspects.storage.ProductQuantity;
import lukomski.filip.food.aspects.supplies.Product;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Component
public class ReceiptDeserializer extends StdDeserializer<Receipt> {


    protected ReceiptDeserializer() {
        super(Receipt.class);
    }

    @Override
    public Receipt deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);

        Receipt receipt = new Receipt();

        Long id = (Long) node.get("id").numberValue();
        receipt.setId(id);

        String name = node.get("name").asText();
        receipt.setName(name);

        Map<Product, ProductQuantity> receiptItems = deserializeShoppingList(node.get("shoppingList"));
        receipt.setReceiptItems(receiptItems);

        return receipt;
    }

    private Map<Product, ProductQuantity> deserializeShoppingList(JsonNode jsonNode) {
        Map<Product, ProductQuantity> receiptItems = new HashMap<>();
        jsonNode.forEach(servingNode -> {
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                Product product = objectMapper.treeToValue(servingNode.get("product"), Product.class);
                ProductQuantity productQuantity = objectMapper.treeToValue(servingNode.get("productQuantity"), ProductQuantity.class);
                receiptItems.put(product, productQuantity);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        });

        return receiptItems;
    }
}
