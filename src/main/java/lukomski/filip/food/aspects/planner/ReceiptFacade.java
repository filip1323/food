package lukomski.filip.food.aspects.planner;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReceiptFacade {
    @Getter
    private ReceiptRepository repository;

    @Autowired
    public ReceiptFacade(ReceiptRepository repository) {
        this.repository = repository;
    }

    public Receipt add(Receipt receipt) {
        return repository.save(receipt);
    }

    public Iterable<Receipt> getAll() {
        return this.repository.findAll();
    }

    public void update(Receipt receipt, Receipt receipt1) {
        throw new UnsupportedOperationException();
//        factory.update(existing, incoming);
//        repository.save(existing);
//        return existing;
    }
}
