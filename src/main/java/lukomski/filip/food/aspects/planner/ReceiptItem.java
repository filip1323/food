package lukomski.filip.food.aspects.planner;

import lombok.Data;
import lukomski.filip.food.aspects.storage.ProductQuantity;
import lukomski.filip.food.aspects.supplies.Product;

@Data
public class ReceiptItem {
    public Product product;
    public ProductQuantity productQuantity;

    public ReceiptItem(Product product, ProductQuantity productQuantity) {
        this.product = product;
        this.productQuantity = productQuantity;
    }
}
