package lukomski.filip.food.aspects.planner;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ReceiptRepository extends CrudRepository<Receipt, Long> {
    Optional<Receipt> findById(Long id);
}
