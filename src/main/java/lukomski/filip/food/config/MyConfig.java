package lukomski.filip.food.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class MyConfig extends WebMvcConfigurationSupport {
//    @Override
//    public FormattingConversionService mvcConversionService() {
//        FormattingConversionService f = super.mvcConversionService();
//        f.addConverter(QuantityStandard.class, String.class, new QuantityStandard.ProductStandardToStringConverter());
//        f.addConverter(String.class, QuantityStandard.class, new QuantityStandard.StringToProductStandardConverter());
//        return f;
//    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowedOrigins("http://localhost:4200/");
            }
        };
    }
//    @Override
//    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
//        Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
//        builder.indentOutput(true);
//        converters.add(new MappingJackson2HttpMessageConverter(builder.build()));
//    }

}