package lukomski.filip.food.config.auth;

public enum Role {
  ;
  public static final String CREATE = "CREATE";
  public static final String READ = "READ";
  public static final String UPDATE = "UPDATE";
  public static final String DELETE = "DELETE";
}
