//package lukomski.filip.food.config.auth;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Profile;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//
//@Configuration
//@EnableWebSecurity
//@Profile({"development", "production", "default"})
//public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
////  @Override
////  protected void configure(HttpSecurity http) throws Exception {
////    http
////        .authorizeRequests()
////        .antMatchers("/").permitAll()
////        .anyRequest().authenticated()
////        .and()
////        .logout()
////        .permitAll()
////        .and()
////        .httpBasic()
////        .and()
////        .csrf().disable();
////  }
////
////  @Autowired
////  public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
////    auth
////        .inMemoryAuthentication()
////        .withUser("user")
////        .password("password")
////        .roles(
////            Role.CREATE,
////            Role.READ,
////            Role.UPDATE,
////            Role.DELETE);
////  }
//}