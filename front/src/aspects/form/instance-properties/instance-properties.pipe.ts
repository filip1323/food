import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'properties'
})
export class InstancePropertiesPipe implements PipeTransform {
  transform(instance, args: String[]): any {
    const properties = [];
    for (const key in instance) {
      if (instance.hasOwnProperty(key)) {
        properties.push({key: key, value: instance[key]});
      }
    } // TODO encapsualte
    return properties;
  }
}
