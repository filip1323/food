import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { FoodValidatorDirective } from './validator/food-validator.directive';
import { FormGroupComponent } from './form-group/form-group.component';
import { FormContainerComponent } from './wizard/form-container.component';
import { InstancePropertiesPipe } from './instance-properties/instance-properties.pipe';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule,
  ],
  declarations: [
    FoodValidatorDirective,
    FormGroupComponent,
    FormContainerComponent,
    InstancePropertiesPipe,
  ],
  exports: [
    FoodValidatorDirective,
    FormGroupComponent,
    FormContainerComponent,
    InstancePropertiesPipe,
  ],
  providers: []
})
export class FoodFormModule {
}
