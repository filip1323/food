import {
  AfterContentInit,
  Component,
  ContentChildren,
  EventEmitter,
  forwardRef,
  Input,
  Output,
  QueryList
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormGroupComponent } from '../form-group/form-group.component';
import { FoodValidatorsService } from '../validator/food-validator.service';
import { Observable } from 'rxjs/Observable';
@Component({
  selector: 'food-form-container',
  templateUrl: 'form-container.component.html',
  providers: []
})
export class FormContainerComponent implements AfterContentInit {
  @Input('formGroup')
  public formGroup: FormGroup;

  @Input('endpoint')
  public endpoint: String;

  @Output('onSubmit')
  public onSubmit = new EventEmitter();

  @ContentChildren(forwardRef(() => FormGroupComponent))
  formGroups: QueryList<FormGroupComponent>;

  public combinedValidationState: Observable<Boolean>;

  constructor(private validatorsService: FoodValidatorsService) {
  }

  ngAfterContentInit(): void {
    this.combinedValidationState = this.validatorsService.getCombinedValidState(this.formGroups);
  }

}
