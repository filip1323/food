import { Injectable, QueryList } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ValidationResponse } from './validation-response.model';
import { Subject } from 'rxjs/Subject';
import { FormGroupComponent } from '../form-group/form-group.component';
@Injectable()
export class FoodValidatorsService {

  constructor(private http: Http) {
  }

  public validate(serviceUrl: String, value: any): Observable<ValidationResponse> {
    return this.http
      .post(`${serviceUrl}`, value, {headers: this.getHeaders()})
      .map(this.mapReponse)
      .catch(this.handleError)
  }

  private mapReponse(res: Response): ValidationResponse {
    return res.json();
  }


  private getHeaders() {
    const headers = new Headers();
    headers.append('Accept', 'application/json');
    return headers;
  }

  private handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  public getCombinedValidState(formGroups: QueryList<FormGroupComponent>): Observable<Boolean> {
    const combinedStateSubject = new Subject<Boolean>();
    let lastCombinedState: Boolean = false;
    const statesMap: any = {};
    formGroups.forEach((formGroup, index, array) => {
      if (formGroup.validator) {
        formGroup.validator.validationState.subscribe(state => {
          statesMap[state.fieldName] = state.valid;
          const currentCombinedState = this.calculateCombinedState(statesMap);
          this.emitIfDiffers(lastCombinedState, currentCombinedState, combinedStateSubject);
          lastCombinedState = currentCombinedState;
        });
      }
    });
    return combinedStateSubject.asObservable();
  }

  private calculateCombinedState(statesMap: any): Boolean {
    for (const key in statesMap) {
      if (statesMap.hasOwnProperty(key)) {
        if (statesMap[key] !== true) {
          return false;
        }
      }
    }
    return true;
  }

  private emitIfDiffers(lastState: Boolean, currentState: Boolean, combinedStateSubject: Subject<Boolean>) {
    if (lastState !== currentState) {
      combinedStateSubject.next(currentState);
    }
  }
}
