export class ValidationResponse {
  public valid: boolean;
  public allowEmptyValue: boolean;
  public isEmpty: boolean;
  public responseMessages: string[];
  public isTouched = true;

  constructor(response: ValidationResponse, isTouched: boolean) {
    this.valid = response.valid;
    this.allowEmptyValue = response.allowEmptyValue;
    this.isEmpty = response.isEmpty;
    this.responseMessages = response.responseMessages;
    this.isTouched = isTouched;
  }

  public isValid() {
    return this.valid && ( this.allowEmptyValue || !this.isTouched || !this.isEmpty );
  }
}
