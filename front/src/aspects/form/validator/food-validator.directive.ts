import { Directive, forwardRef, Inject, Input, OnInit } from '@angular/core';
import { AbstractControl, NG_ASYNC_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { config } from '../../../app/app.config';
import { FoodValidatorsService } from './food-validator.service';
import { ValidationResponse } from './validation-response.model';
import { Subject } from 'rxjs/Subject';
import { FormContainerComponent } from '../wizard/form-container.component';
import { ValidationState } from './validation-state.model';
@Directive({
  selector: '[food-validatable][formControlName]',
  providers: [
    {
      provide: NG_ASYNC_VALIDATORS,
      useExisting: forwardRef(() => FoodValidatorDirective), multi: true
    },
    FoodValidatorsService
  ]
})
export class FoodValidatorDirective implements Validator, OnInit {
  @Input('formControlName')
  fieldEndpoint: string;
  private actionEndpoint = 'validate';
  private serviceUrl: String;
  public validationErrors: Observable<ValidationResponse>;
  public validationState: Observable<ValidationState>;
  private validationErrorsSubject: Subject<ValidationResponse>;
  private validationStateSubject: Subject<ValidationState>;
  private form: FormContainerComponent;

  constructor(public service: FoodValidatorsService, @Inject(forwardRef(() => FormContainerComponent)) form: FormContainerComponent) {
    this.form = form;
    this.validationErrorsSubject = new Subject<ValidationResponse>();
    this.validationStateSubject = new Subject<ValidationState>();
    this.validationErrors = this.validationErrorsSubject.asObservable();
    this.validationState = this.validationStateSubject.asObservable();
  }

  ngOnInit(): void {
    this.serviceUrl = config.API + '/' + this.form.endpoint + '/' + this.actionEndpoint + '/' + this.fieldEndpoint;
  }

  validate(c: AbstractControl): ValidationErrors {
    const serviceResponse = this.service.validate(this.serviceUrl, c.value);
    serviceResponse.subscribe(validationResponse => {
      validationResponse = new ValidationResponse(validationResponse, c.touched);
      this.validationErrorsSubject.next(validationResponse);
      this.validationStateSubject.next({valid: validationResponse.isValid(), fieldName: this.fieldEndpoint});
    });
    return serviceResponse.first();
  }

  registerOnValidatorChange(fn: () => void): void {
  }

}
