export interface ValidationState {
  valid: boolean;
  fieldName: string;
}
