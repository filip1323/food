import { AfterContentInit, Component, ContentChild, forwardRef } from '@angular/core';
import { FoodValidatorDirective } from '../validator/food-validator.directive';
import { ValidationResponse } from '../validator/validation-response.model';
@Component({
  selector: 'food-form-group',
  templateUrl: 'form-group.component.html',
  styleUrls: ['form-group.component.css']
})
export class FormGroupComponent implements AfterContentInit {
  @ContentChild(forwardRef(() => FoodValidatorDirective))
  validator: FoodValidatorDirective;

  validationResponse: ValidationResponse = null;

  ngAfterContentInit(): void {
    if (this.validator !== undefined) {
      this.validator.validationErrors.subscribe(response => {
        this.validationResponse = response;
      });
    }
  }
}
