import { ControlValueAccessor } from '@angular/forms';
import { OnInit } from '@angular/core';

export abstract class FoodFormControl<T> implements ControlValueAccessor, OnInit {

  protected _value: T;

  // protected onTouchedCallback: () => {};
  // protected onChangeCallback: (_: any) => {};

  protected onTouchedCallback = () => {};
  protected onChangeCallback = (_: any) => {};

  constructor() {
  }

  ngOnInit() {
    this.initializeModel(model => this._value = model);
  }

  protected onChange(event) {
    this._value = event.target.value;
    this.onChangeCallback(event.target.value);
  }

  writeValue(newValue: any) {
    if (newValue !== this._value) {
      this._value = newValue;
    }
  }

  set value(newValue: T) {
    if (newValue !== this._value) {
      this._value = newValue;
    }
  }

  get value(): T {
    return this._value;
  }

  registerOnChange(fn: any) {
    this.onChangeCallback = fn;
    this.initializeModel((value) => {
      this._value = value;
      this.onChangeCallback(this._value);
    });
  }

  registerOnTouched(fn: any) {
    this.onTouchedCallback = fn;
  }

  protected initializeModel(populateModel: (value: any) => void): void {
  }
}
