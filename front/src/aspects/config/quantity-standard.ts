import { Serializable } from '../service/serializable.interface';


export class QuantityStandard implements Serializable<QuantityStandard> {
  public id: string;
  public name: string;
  public unit: string;

  constructor() {
  }

  newInstance(): QuantityStandard {
    return new QuantityStandard();
  }
}
