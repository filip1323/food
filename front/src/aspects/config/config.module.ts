import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { FoodConfigService } from './config.service';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
  ],
  declarations: [
  ],
  exports: [
  ],
  providers: [FoodConfigService]
})
export class FoodConfigModule {
}
