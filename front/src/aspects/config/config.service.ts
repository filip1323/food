import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { ServiceUtils } from '../service/service-utils';
import { Codec } from '../service/codec';
import { QuantityStandard } from './quantity-standard';
import { config } from '../../app/app.config';

@Injectable()
export class FoodConfigService {
  private serviceUrl = `${config.API}/config`;
  private quantityStandardsUrl = `${this.serviceUrl}/quantityStandards`;

  constructor(private http: Http) {
  }

  public loadQuantityStandardsConfig(onLoad: (quantityStandards: QuantityStandard[]) => void) {
    const reference = new QuantityStandard();
    this.http
      .get(this.quantityStandardsUrl, ServiceUtils.standardOptions())
      .map(ServiceUtils.toArray)
      .map(v => Codec.genericsToConcretes(v, reference))
      .catch(err => ServiceUtils.handleError(err))
      .subscribe(onLoad);
  }

}
