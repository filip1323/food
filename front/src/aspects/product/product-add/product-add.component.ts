import { FoodProductService } from '../product.service';
import { FormBuilder } from '@angular/forms';
import { AfterViewInit, Component, forwardRef, ViewChild } from '@angular/core';
import { BSModalContext, Modal } from 'angular2-modal/plugins/bootstrap';
import { ProductModalComponent } from '../product-modal/product-modal.component';
import { overlayConfigFactory } from 'angular2-modal';
import { Product } from '../product.model';
import { FoodValidatorsService } from '../../form/validator/food-validator.service';
import { FormContainerComponent } from '../../form/wizard/form-container.component';
import { FoodConfigService } from '../../config/config.service';

@Component({
  styleUrls: ['../style.css'],
  templateUrl: './product-add.component.html',
  providers: [FoodProductService, FoodValidatorsService],
  selector: 'food-product-add-form'
})
export class ProductsAddFormComponent implements AfterViewInit {

  @ViewChild(forwardRef(() => FormContainerComponent))
  formContainer: FormContainerComponent;

  public reactiveForm;
  private formValidationState: Boolean;


  constructor(public service: FoodProductService,
              public modal: Modal,
              public configService: FoodConfigService,
              fb: FormBuilder) {
    this.reactiveForm = fb.group({
      'name': [''],
      'fatPerUnit': [''],
      'carboPerUnit': [''],
      'proteinPerUnit': [''],
      'defaultStandard': ['']
    });
    this.reactiveForm.controls.defaultStandard.setValue('NUMBER');
  }

  onSubmit() {
    this.onFormValidate(() => {
      const product = this.readProduct();
      this.modal.open(
        ProductModalComponent,
        overlayConfigFactory(
          {
            title: 'Potwierdzasz, że chcesz dodać produkt o podanych parametrach?',
            product: product
          },
          BSModalContext))
        .then(
          dialog => dialog.result.then(
            result => this.processUserResponse(result, product)));
    });
  }

  onFormValidate(onValidate: () => void) {
    for (const key in this.reactiveForm.controls) {
      if (this.reactiveForm.controls.hasOwnProperty(key)) {
        this.reactiveForm.controls[key].markAsTouched(true);
        this.reactiveForm.controls[key].updateValueAndValidity();
      }
    }
    /** FIXME add 'pending' state to form validation
     * onValidate() should not be fired after arbitrary period
     * this event should be handled in functional way
    */
    setTimeout(() => {
      if (this.formValidationState) {
        onValidate();
      }
    }, 1000);
  }

  readProduct(): Product {
    const product = new Product();
    product.name = this.reactiveForm.controls.name.value;
    product.defaultStandard = this.reactiveForm.controls.defaultStandard.value;
    product.fatPerUnit = this.reactiveForm.controls.fatPerUnit.value;
    product.proteinPerUnit = this.reactiveForm.controls.proteinPerUnit.value;
    product.carboPerUnit = this.reactiveForm.controls.carboPerUnit.value;
    return product;
  }

  processUserResponse(userConfirmed: Boolean, product: Product) {
    if (userConfirmed) {
      this.service.add(product, status => console.log('Success: ' + status));
    }
  }

  ngAfterViewInit(): void {
    this.formContainer.combinedValidationState.subscribe(state => this.formValidationState = state);
  }
}
