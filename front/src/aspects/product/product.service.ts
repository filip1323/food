import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Product } from './product.model';
import 'rxjs/Rx';
import { config } from '../../app/app.config';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';
import { ServiceUtils } from '../service/service-utils';
import { Codec } from '../service/codec';

@Injectable()
export class FoodProductService {
  private serviceUrl = config.API + '/product';
  private allProductsSubject: Subject<Product[]>;
  private reference = new Product();
  constructor(private http: Http) {
    this.allProductsSubject = new Subject<Product[]>();
  }

  subscribe(next?: (value: Product[]) => void, error?: (error: any) => void, complete?: () => void): Subscription {
    const subscribtion = this.allProductsSubject.asObservable().subscribe(next, error, complete);
    this.refreshProducts();
    return subscribtion
  }

  refreshProducts() {
    this.http
      .get(`${this.serviceUrl}`,)
      .map(ServiceUtils.toArray)
      .map(v => Codec.genericsToConcretes(v, this.reference))
      .catch(err => ServiceUtils.handleError(err))
      .subscribe(products => this.allProductsSubject.next(products));
  }

  add(product: Product, next: (status: Boolean) => void) {
    return this.http
      .post(`${this.serviceUrl}`, product, ServiceUtils.standardOptions())
      .map(ServiceUtils.toSuccessStatus)
      .catch(err => ServiceUtils.handleError(err, () => next(false)))
      .subscribe(next)
  }

  remove(product: Product, next: (status: Boolean) => void) {
    return this.http
      .delete(`${this.serviceUrl}/${product.id}`, ServiceUtils.standardOptions())
      .map(ServiceUtils.toSuccessStatus)
      .catch(err => ServiceUtils.handleError(err, () => next(false)))
      .subscribe((status) => {
        next(status);
        this.refreshProducts()
      });
  }
}
