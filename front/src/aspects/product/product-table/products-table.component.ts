import { Component, OnInit } from '@angular/core';
import { Product } from '../product.model';
import { FoodProductService } from '../product.service';
import { BSModalContext, Modal } from 'angular2-modal/plugins/bootstrap';
import { ProductModalComponent } from '../product-modal/product-modal.component';
import { overlayConfigFactory } from 'angular2-modal/esm';

@Component({
  styleUrls: ['../style.css'],
  templateUrl: './products-table.component.html',
  providers: [FoodProductService],
  selector: 'food-products-list'
})
export class ProductsTableComponent implements OnInit {

  public products: Array<Product> = [];

  constructor(public service: FoodProductService,
              public modal: Modal) {
  }

  ngOnInit() {
    this.service
      .subscribe(p => {
        this.products = p
      });
    this.service.refreshProducts();
  }

  public deleteProduct(product: Product) {
    this.modal.open(
      ProductModalComponent,
      overlayConfigFactory(
        {
          title: 'Potwierdzasz, że chcesz usunąć produkt o podanych parametrach?',
          product: product
        },
        BSModalContext))
      .then(
        dialog => dialog.result.then(
          result => this.processUserResponse(result, product)));
  }

  processUserResponse(userConfirmed: Boolean, product: Product) {
    if (userConfirmed) {
      this.service.remove(product, status => console.log('Success: ' + status));
    }
  }
}
