import { QuantityStandard } from '../config/quantity-standard';

export class ProductQuantity {

  constructor(public standard?: QuantityStandard, public value?: number) {
  }

  public toString() {
    return `${this.value} ${this.standard.unit}`
  }
}
