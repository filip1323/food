import { Product } from './product.model';
import { ProductQuantity } from './product-quantity.model';
import { Serializable } from '../service/serializable.interface';

export class ProductServing implements Serializable<ProductServing> {
  constructor(public product?: Product, public productQuantity?: ProductQuantity) {
  }

  newInstance(): ProductServing {
    return new ProductServing;
  }
}
