import { Component, forwardRef } from '@angular/core';
import { FoodProductService } from '../product.service';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { FoodFormControl } from '../../form/form-control/form-control.component';
import { FoodConfigService } from '../../config/config.service';
import { QuantityStandard } from '../../config/quantity-standard';

@Component({
  templateUrl: './quantity-standard.component.html',
  selector: 'food-quantity-standard',
  providers: [
    FoodProductService,
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => QuantityStandardComponent),
      multi: true,
    },
    FoodConfigService
  ]
})
export class QuantityStandardComponent extends FoodFormControl<QuantityStandard> {
  quantityStandards: QuantityStandard[];

  constructor(private configService: FoodConfigService) {
    super();
  }

  protected initializeModel(populateModel: (value: any) => void): void {
    this.configService.loadQuantityStandardsConfig(qs => {
      this.quantityStandards = qs;
      populateModel(qs[0]);
    });
  }

  compareById(first: QuantityStandard, second: QuantityStandard) {
    return first && second && first.id === second.id;
  }
}
