import { Component } from '@angular/core';
import { DialogRef, ModalComponent } from 'angular2-modal';
import { BSModalContext } from 'angular2-modal/plugins/bootstrap';
import { Product } from '../product.model';

export class ProductModalData extends BSModalContext {
  public title: String;
  public product: Product;
}

@Component({
  styleUrls: ['product-modal.component.css'],
  templateUrl: 'product-modal.component.html'
})
export class ProductModalComponent implements ModalComponent<ProductModalData> {
  public context: ProductModalData;

  constructor(public dialog: DialogRef<ProductModalData>) {
    this.context = dialog.context;
  }

  public onCancel() {
    this.dialog.close(false);
  }

  public onConfirm() {
    this.dialog.close(true);
  }
}
