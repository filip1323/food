import { Component, forwardRef } from '@angular/core';
import { FoodProductService } from '../product.service';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { ProductQuantity } from '../product-quantity.model';
import { FoodFormControl } from '../../form/form-control/form-control.component';

@Component({
  templateUrl: './product-quantity.component.html',
  selector: 'food-product-quantity',
  providers: [
    FoodProductService,
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ProductQuantityComponent),
      multi: true,
    }
  ]
})
export class ProductQuantityComponent extends FoodFormControl<ProductQuantity> {
  constructor() {
    super();
  }

  protected initializeModel(populateModel: (value: any) => void): void {
    populateModel(new ProductQuantity(undefined, 1));
  }

  private onStandardChange(event) {
    this._value.standard = event.target.value;
    this.onChangeCallback(this._value);
  }

  private onQuantityChange(event) {
    this._value.value = event.target.value;
    this.onChangeCallback(this._value);
  }
}
