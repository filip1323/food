import { Serializable } from '../service/serializable.interface';
import { QuantityStandard } from '../config/quantity-standard';

export class Product implements Serializable<Product> {
  id?: number;
  name: string;

  description?: string;
  defaultStandard: QuantityStandard;
  fatPerUnit: number;

  carboPerUnit: number;
  proteinPerUnit: number;
  openSinceArrival?: boolean;

  newInstance(): Product {
    return new Product();
  }


}
