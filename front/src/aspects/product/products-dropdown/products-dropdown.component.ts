import { Component, forwardRef } from '@angular/core';
import { Product } from '../product.model';
import { FoodProductService } from '../product.service';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { FoodFormControl } from '../../form/form-control/form-control.component';

@Component({
  templateUrl: './products-dropdown.component.html',
  selector: 'food-products-dropdown',
  providers: [
    FoodProductService,
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ProductsDropdownComponent),
      multi: true,
    }
  ]
})
export class ProductsDropdownComponent extends FoodFormControl<Product> {
  public products: Array<Product> = [];

  constructor(public service: FoodProductService) {
    super();
  }

  protected initializeModel(populateModel: (value: any) => void): void {
    let onInit = true;
    this.service
      .subscribe(p => {
        this.products = p;
        if (onInit) {
          populateModel(this.products[0]);
          onInit = false;
        }
      });
  }
  compareById(first: Product, second: Product) {
    return first && second && first.id === second.id;
  }
  protected onChange(event) {
    this.onChangeCallback(this._value);
  }

}
