import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductPage } from '../../pages/product/product.page';
import { FoodProductService } from './product.service';
import { ProductsAddFormComponent } from './product-add/product-add.component';
import { ProductsTableComponent } from './product-table/products-table.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { FoodFormModule } from '../form/form.module';
import { FormContainerComponent } from '../form/wizard/form-container.component';
import { ProductModalComponent } from './product-modal/product-modal.component';
import { ModalModule } from 'angular2-modal';
import { BootstrapModalModule } from 'angular2-modal/plugins/bootstrap';
import { ProductsDropdownComponent } from './products-dropdown/products-dropdown.component';
import { ProductQuantityComponent } from './product-quantity/product-quantity.component';
import { QuantityStandardComponent } from './quantity-standard/quantity-standard.component';
import { FoodConfigModule } from '../config/config.module';
@NgModule({
  imports: [
    FoodFormModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule,
    FoodConfigModule,
    ModalModule.forRoot(),
    BootstrapModalModule,
  ],
  declarations: [
    ProductPage,
    ProductsAddFormComponent,
    ProductsTableComponent,
    ProductModalComponent,
    ProductsDropdownComponent,
    QuantityStandardComponent,
    ProductQuantityComponent,
  ],
  entryComponents: [
    ProductModalComponent,
  ],
  exports: [
    ProductPage,
    ProductsAddFormComponent,
    ProductsTableComponent,
    ProductsDropdownComponent,
    QuantityStandardComponent,
    ProductQuantityComponent,
  ],
  providers: [
    FoodProductService,
    FormContainerComponent,
  ]
})
export class FoodProductModule {
}
