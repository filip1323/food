import { Serializable } from './serializable.interface';

export class Codec {

  static deserializeConcrete<T extends Serializable<T>>(generic: Object, reference: T): T {
    const deserialized = reference.newInstance();
    for (const key in generic) {
      if (generic.hasOwnProperty(key)) {
        deserialized[key] = generic[key];
      }
    }
    return deserialized;
  }

  static genericsToConcretes<T extends Serializable<T>>(generics: Object[], reference: T): T[] {
    const concreteInstances = [];
    generics.forEach(obj => concreteInstances.push(Codec.deserializeConcrete(obj, reference)));
    return concreteInstances;
  }

  static concretesGenericPropertyToConcreteProperty<T extends Serializable<T>, E extends Serializable<E>>
                                          (concretes: E[], propertyKey: string, propertyReference: T): E[] {
    concretes.forEach(obj => {
      obj[propertyKey] = this.deserializeConcrete(obj[propertyKey], propertyReference);
    });
    return concretes;
  }

  static concretesGenericPropertyArrayToConcretePropertyArray<T extends Serializable<T>, E extends Serializable<E>>
                                          (concretes: E[], propertyArrayKey: string, propertyReference: T): E[] {
    concretes.forEach(obj => {
      const concretePropertyArray = [];
      obj[propertyArrayKey]
        .forEach(property => concretePropertyArray.push(this.deserializeConcrete(property, propertyReference)))
      obj[propertyArrayKey] = concretePropertyArray;
    });
    return concretes;
  }
}
