export interface Serializable<T> {
  newInstance(): T;
}
