import { Observable } from 'rxjs/Observable';
import { Headers, Response } from '@angular/http';

export class ServiceUtils {


  public static getHeaders() {
    const headers = new Headers();
    headers.append('Accept', 'application/json');
    return headers;
  }

  public static standardOptions() {
    return {headers: ServiceUtils.getHeaders()}
  }

  public static handleError(error: Response | any, onError?: () => void) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    if (onError) {
      onError();
    }
    return Observable.throw(errMsg);
  }

  public static toSuccessStatus(res: Response): Boolean {
    return res.status === 200;
  }


  public static toArray(res: Response): Object[] {
    const body = res.json();
    return body || [];
  }
}
