import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { FoodFormModule } from '../form/form.module';
import { FormContainerComponent } from '../form/wizard/form-container.component';
import { ModalModule } from 'angular2-modal';
import { BootstrapModalModule } from 'angular2-modal/plugins/bootstrap';
import { FoodReceiptService } from './receipt.service';
import { ReceiptPage } from '../../pages/receipt/receipt.page';
import { ReceiptAddFormComponent } from './product-add/receipt-add.component';
import { ReceiptsTableComponent } from './receipts-table/receipts-table.component';
import { FoodProductModule } from '../product/product.module';
import { ReceiptItemModalComponent } from './receipt-item-modal/receipt-item-modal.component';
import { ReceiptModalComponent } from './receipt-modal/receipt-modal.component';
@NgModule({
  imports: [
    FoodFormModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule,
    ModalModule.forRoot(),
    BootstrapModalModule,
    FoodProductModule,
  ],
  declarations: [
    ReceiptPage,
    ReceiptAddFormComponent,
    ReceiptsTableComponent,
    ReceiptModalComponent,
    ReceiptItemModalComponent
  ],
  entryComponents: [
    ReceiptModalComponent,
    ReceiptItemModalComponent,
  ],
  exports: [
    ReceiptPage,
    ReceiptAddFormComponent,
    ReceiptsTableComponent,
  ],
  providers: [
    FoodReceiptService,
    FormContainerComponent,
  ]
})
export class FoodReceiptModule {
}
