import { ProductServing } from '../product/product-serving.model';
import { Serializable } from '../service/serializable.interface';

export class Receipt implements Serializable<Receipt> {
  public id: number;
  public name: string;
  public shoppingList: ProductServing[]

  newInstance(): Receipt {
    return new Receipt();
  }
}
