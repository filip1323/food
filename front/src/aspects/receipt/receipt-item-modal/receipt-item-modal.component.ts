import { Component } from '@angular/core';
import { DialogRef, ModalComponent } from 'angular2-modal';
import { BSModalContext } from 'angular2-modal/plugins/bootstrap';
import { ProductServing } from '../../product/product-serving.model';

export class ReceiptItemModalData extends BSModalContext {
}

@Component({
  styleUrls: ['receipt-item-modal.component.css'],
  templateUrl: 'receipt-item-modal.component.html',
  providers: []
})
export class ReceiptItemModalComponent implements ModalComponent<ReceiptItemModalData> {
  public context: ReceiptItemModalData;
  private productServing: ProductServing = new ProductServing();
  constructor(public dialog: DialogRef<ReceiptItemModalData>) {
    this.context = dialog.context;
  }

  public onCancel() {
    this.dialog.close();
  }

  public onConfirm() {
    this.dialog.close(this.readReceiptItem());
  }

  private readReceiptItem(): ProductServing {
    return this.productServing;
  }
}
