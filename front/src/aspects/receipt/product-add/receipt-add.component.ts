import { FormBuilder } from '@angular/forms';
import { AfterViewInit, Component, forwardRef, ViewChild } from '@angular/core';
import { FoodValidatorsService } from '../../form/validator/food-validator.service';
import { FormContainerComponent } from '../../form/wizard/form-container.component';
import { FoodReceiptService } from '../receipt.service';
import { Modal, overlayConfigFactory } from 'angular2-modal';
import { ReceiptItemModalComponent } from '../receipt-item-modal/receipt-item-modal.component';
import { BSModalContext } from 'angular2-modal/plugins/bootstrap';
import { ProductServing } from '../../product/product-serving.model';
@Component({
  styleUrls: ['../style.css'],
  templateUrl: './receipt-add.component.html',
  providers: [FoodReceiptService, FoodValidatorsService],
  selector: 'food-receipt-add-form'
})
export class ReceiptAddFormComponent implements AfterViewInit {
  @ViewChild(forwardRef(() => FormContainerComponent))
  formContainer: FormContainerComponent;

  private formValidationState: Boolean;

  public reactiveForm;

  private productServings: ProductServing[] = [];

  constructor(public service: FoodReceiptService,
              public modal: Modal,
              fb: FormBuilder) {
    this.reactiveForm = fb.group({
      'name': ['']
    });
  }

  public addReceiptItem() {
    this.modal.open(
      ReceiptItemModalComponent,
      overlayConfigFactory(
        {},
        BSModalContext)).then(dialog =>
      dialog.result.then(this.processAddReceiptItemModalResponse));
  }

  private processAddReceiptItemModalResponse = (response?: ProductServing) => {
    if (response) {
      this.productServings.push(response);
    }
  };

  ngAfterViewInit(): void {
    this.formContainer.combinedValidationState.subscribe(state => this.formValidationState = state);
  }
}
