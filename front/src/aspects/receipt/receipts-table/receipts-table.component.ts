import { Component, OnInit } from '@angular/core';
import { BSModalContext, Modal } from 'angular2-modal/plugins/bootstrap';
import { FoodReceiptService } from '../receipt.service';
import { Receipt } from '../receipt.model';
import { overlayConfigFactory } from 'angular2-modal';
import { ReceiptModalComponent } from '../receipt-modal/receipt-modal.component';

@Component({
  styleUrls: ['../style.css'],
  templateUrl: './receipts-table.component.html',
  providers: [FoodReceiptService],
  selector: 'food-receipts-list'
})
export class ReceiptsTableComponent implements OnInit {

  public receipts: Array<Receipt> = [];

  constructor(public service: FoodReceiptService,
              public modal: Modal) {
  }

  ngOnInit() {
    this.service
      .subscribe(p => {
        this.receipts = p
      });
    this.service.refreshReceipts();
  }

  public showReceipt(receipt: Receipt) {
    this.modal.open(
      ReceiptModalComponent,
      overlayConfigFactory(
        {
          title: receipt.name,
          receipt: receipt,
          showCloseButton: true,
          showConfirmButton: false
        },
        BSModalContext))
  }

  public editReceipt(receipt: Receipt) {

  }

  public deleteReceipt(receipt: Receipt) {
    this.modal.open(
      ReceiptModalComponent,
      overlayConfigFactory(
        {
          title: 'Potwierdzasz, że chcesz usunąć przepis o podanych parametrach?',
          receipt: receipt,
          showConfirmButton: true,
          showCancelButton: true
        },
        BSModalContext))
      .then(
        dialog => dialog.result.then(
          result => this.processUserResponse(result, receipt)));
  }

  processUserResponse(userConfirmed: Boolean, receipt: Receipt) {
    if (userConfirmed) {
      this.service.remove(receipt);
    }
  }
}
