import { Component } from '@angular/core';
import { DialogRef, ModalComponent } from 'angular2-modal';
import { BSModalContext } from 'angular2-modal/plugins/bootstrap';
import { Receipt } from '../receipt.model';

export class ReceiptModalData extends BSModalContext {
  public title: String;
  public receipt: Receipt;
  public showCancelButton: boolean;
  public showConfirmButton: boolean;
}

@Component({
  styleUrls: ['receipt-modal.component.css'],
  templateUrl: 'receipt-modal.component.html'
})
export class ReceiptModalComponent implements ModalComponent<ReceiptModalData> {
  public context: ReceiptModalData;

  constructor(public dialog: DialogRef<ReceiptModalData>) {
    this.context = dialog.context;
  }

  public onCancel() {
    this.dialog.close(false);
  }

  public onConfirm() {
    this.dialog.close(true);
  }
}
