import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/Rx';
import { config } from '../../app/app.config';
import { Subject } from 'rxjs/Subject';
import { Receipt } from './receipt.model';
import { Subscription } from 'rxjs/Subscription';
import { ServiceUtils } from '../service/service-utils';
import { Codec } from '../service/codec';
import { ProductServing } from '../product/product-serving.model';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class FoodReceiptService {
  private serviceUrl = config.API + '/receipt';
  private allReceiptsSubject: Subject<Receipt[]>;
  private reference: Receipt = new Receipt();
  private servingReference: ProductServing = new ProductServing();
  constructor(private http: Http) {
    this.allReceiptsSubject = new Subject<Receipt[]>();
  }

  subscribe(next?: (value: Receipt[]) => void, error?: (error: any) => void, complete?: () => void): Subscription {
    const subscribtion = this.allReceiptsSubject.asObservable().subscribe(next, error, complete);
    this.refreshReceipts();
    return subscribtion;
  }

  refreshReceipts() {
    this.http
      .get(`${this.serviceUrl}`, ServiceUtils.standardOptions())
      .map(ServiceUtils.toArray)
      .map(receipts => Codec.genericsToConcretes(receipts, this.reference))
      .map(receipts => Codec.concretesGenericPropertyArrayToConcretePropertyArray(receipts,
        'shoppingList', this.servingReference))
      .catch(err => ServiceUtils.handleError(err))
      .subscribe(receipts => this.allReceiptsSubject.next(receipts));
  }

  remove(receipt: Receipt, next?: (status: Boolean) => void) {
    return this.http
      .delete(`${this.serviceUrl}/${receipt.id}`, ServiceUtils.standardOptions())
      .map(ServiceUtils.toSuccessStatus)
      .catch(err => ServiceUtils.handleError(err, () => next(false)))
      .subscribe((status) => {
        if (next) {
          next(status);
        }
        this.refreshReceipts()
      });
  }


}
