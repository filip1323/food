import { Component } from '@angular/core';

@Component({
  selector: 'food-navbar',
  template: `
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                  data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/home">Home</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li class="dropdown">
              <a href="/product" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Produkty
                <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="/product">Baza danych <span class="glyphicon glyphicon-cloud pull-right"
                                                         aria-hidden="true"></span></a></li>
                <li><a href="/product/add">Dodaj nowy <span class="glyphicon glyphicon-plus pull-right"
                                                            aria-hidden="true"></span></a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="/receipt" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Przepisy
                <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="/receipt">Baza danych <span class="glyphicon glyphicon-cloud pull-right"
                                                         aria-hidden="true"></span></a></li>
                <li><a href="/receipt/add">Dodaj nowy <span class="glyphicon glyphicon-plus pull-right"
                                                            aria-hidden="true"></span></a></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  `,
  styleUrls: ['./app.component.css']
})
export class NavbarComponent {
}
