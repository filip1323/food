import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FoodAppComponent } from './app.component';
import { HomePage } from '../pages/home/home.page';
import { FoodHomeModule } from '../aspects/home/home.module';
import { NavbarComponent } from './navbar.component';
import { ProductPage } from '../pages/product/product.page';
import { FoodProductModule } from '../aspects/product/product.module';
import { ProductsAddFormComponent } from '../aspects/product/product-add/product-add.component';
import { ProductsTableComponent } from '../aspects/product/product-table/products-table.component';
import { HttpModule } from '@angular/http';
import { FoodFormModule } from '../aspects/form/form.module';
import { ModalModule } from 'angular2-modal';
import { BootstrapModalModule } from 'angular2-modal/plugins/bootstrap';
import { FoodReceiptModule } from '../aspects/receipt/receipt.module';
import { ReceiptPage } from '../pages/receipt/receipt.page';
import { ReceiptsTableComponent } from '../aspects/receipt/receipts-table/receipts-table.component';
import { ReceiptAddFormComponent } from '../aspects/receipt/product-add/receipt-add.component';
import { FoodConfigModule } from '../aspects/config/config.module';

const appRoutes: Routes = [
  {path: 'home', component: HomePage},
  {
    path: 'product', component: ProductPage,
    children: [
      {path: '', component: ProductsTableComponent},
      {path: 'add', component: ProductsAddFormComponent},
    ]
  },
  {
    path: 'receipt', component: ReceiptPage,
    children: [
      {path: '', component: ReceiptsTableComponent},
      {path: 'add', component: ReceiptAddFormComponent},
    ]
  },
  {path: '', redirectTo: '/home', pathMatch: 'full'}
];

@NgModule({
  declarations: [
    FoodAppComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot(
      appRoutes,
      {enableTracing: true} // <-- debugging purposes only
    ),
    FoodHomeModule,
    FoodProductModule,
    FoodReceiptModule,
    FoodFormModule,
    FoodConfigModule,
    ModalModule.forRoot(),
    BootstrapModalModule
  ],
  providers: [],
  bootstrap: [FoodAppComponent]
})
export class FoodAppModule {
}
