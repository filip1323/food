import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FoodAppService } from './app.service';
import { Modal } from 'angular2-modal/plugins/bootstrap';
import { Overlay } from 'angular2-modal/esm';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [FoodAppService]
})
export class FoodAppComponent implements OnInit {
  public connectionEstablished: Boolean = false;

  constructor(public service: FoodAppService,
              vcRef: ViewContainerRef,
              public modal: Modal,
              overlay: Overlay) {
    overlay.defaultViewContainer = vcRef;
  }

  ngOnInit() {
    this.service
      .ping()
      .subscribe(p => {
        this.connectionEstablished = p;
      });
  }

  onClick() {
    this.modal.alert()
      .showClose(true)
      .title('A simple Alert style modal window')
      .body(`
            <h4>Alert is a classic (title/body/footer) 1 button modal window that
            does not block.</h4>
            <b>Configuration:</b>
            <ul>
                <li>Non blocking (click anywhere outside to dismiss)</li>
                <li>Size large</li>
                <li>Dismissed with default keyboard key (ESC)</li>
                <li>Close wth button click</li>
                <li>HTML content</li>
            </ul>`)
      .open();
  }
}
