import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { config } from './app.config';
@Injectable()
export class FoodAppService {
  private serviceUrl = `${config.API}/ping`;

  constructor(private http: Http) {
  }

  ping(): Observable<Boolean> {
    return this.http
      .get(`${this.serviceUrl}`, {headers: this.getHeaders()})
      .map(this.mapReponse)
      .catch(this.handleError)
  }

  private mapReponse(res: Response): Boolean {
    const body = res.json();
    return body != null;
  }


  private getHeaders() {
    const headers = new Headers();
    headers.append('Accept', 'application/json');
    return headers;
  }

  private handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
